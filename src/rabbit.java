
public class rabbit {
	
		
		// -----------
		// PROPERTIES
		// -----------
		private int xPosition;
		private int yPosition;
		
		
		// -----------
		// CONSTRUCTOR 
		// ------------
		public rabbit(int x,int y) {
			this.xPosition =x;
			this.yPosition =y;
			

		}
		
		// -----------
		// METHODS 
		// ------------
		public void printCurrentPosition() {
			System.out.println("The current position of the rabbit is: ");
		}
		// ACCESSOR METHODS
		public void sayHello() {
			System.out.println("Hello! I am a rabbit!");
		}

		public int getxPosition() {
			xPosition=xPosition+15;
			System.out.println("Rabbit is moving right");
			return xPosition;
		}

		public void setxPosition(int xPosition) {
			this.xPosition = xPosition;
			}

		public int getyPosition() {
			yPosition = yPosition+15;
			System.out.println("Rabbit is moving left");
			return yPosition;
		}

		public void setyPosition(int yPosition) {
			this.yPosition = yPosition;
			}
		
		
		
		
		// Put all your accessor methods in this section.
		
		
	}

